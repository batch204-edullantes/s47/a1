let input1 = document.querySelector('#txt-first-name');
let input2 = document.querySelector('#txt-last-name');
let span1 = document.querySelector('#span-full-name');
let resetBtn = document.querySelector('#reset');

input1.addEventListener('keyup', setFullName);

input2.addEventListener('keyup', setFullName);

function setFullName(e) {
	span1.innerHTML = `${input1.value} ${input2.value}`;
}

resetBtn.addEventListener('click', () => {
	input1.value = '';
	input2.value = '';
	span1.innerHTML = '';
});
